<%@ page import="com.sandun.web.model.User" %>
<%@ page import="com.sandun.web.db.DBConnection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: SANDUN BUDDHIKA
  Date: 3/31/2023
  Time: 11:43 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    if (session.getAttribute("u") != null) {
%>
<html>
<head>
    <title>${u.getName()}'s Home</title>
</head>
<body>
<h1>Welcome Back ${u.getName()}</h1>
<h1>All Users</h1>
<table>

    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Passowrd</th>
    </tr>

    <%
        Connection c = null;
        try {
            c = DBConnection.getConnection();
            ResultSet rs = c.createStatement().executeQuery("SELECT * FROM `user` ;");
            while (rs.next()) {
                if (!rs.getString("id").equals(((User) session.getAttribute("u")).getId())) {
    %>
    <tr>
        <form action="DeleteProcess" method="POST">
            <td><input type="text" readonly name="id" value="<%out.write(rs.getString("id"));%>"/></td>
            <td><input type="text" name="name" value="<%out.write(rs.getString("name"));%>"/></td>
            <td><input type="password" name="password" value="<%out.write(rs.getString("password"));%>"/></td>
            <td>
                <button type="submit" style="background-color: #ff0000;color: white;">Delete</button>
            </td>
        </form>
    </tr>
    <%
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                c.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    %>
</table>
<h1>Update users</h1>
<table>
    <%
        Connection c1 = null;
        try {
            c1 = DBConnection.getConnection();
            ResultSet rs = c1.createStatement().executeQuery("SELECT * FROM `user` ;");
            while (rs.next()) {

    %>
    <tr>
        <form action="UpdateProcess" method="POST">
            <td><input type="text" readonly name="id" value="<%out.write(rs.getString("id"));%>"/></td>
            <td><input type="text" name="name" value="<%out.write(rs.getString("name"));%>"/></td>
            <td><input type="password" name="password" value="<%out.write(rs.getString("password"));%>"/></td>
            <td>
                <button type="submit" style="background-color: #00a4ff;color: white;">Update</button>
            </td>
        </form>
    </tr>
    <%
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                c1.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    %>
</table>
<br>
<br>
<br>
<a href="SignOutProcess">Sign Out</a>
</body>
</html>

<%
    } else {
        response.sendRedirect("index.jsp");
    }

%>