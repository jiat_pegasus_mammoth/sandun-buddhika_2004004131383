package com.sandun.web.model;

import com.sandun.web.db.DBConnection;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

@WebServlet(name = "DeleteProcess", value = "/DeleteProcess")
public class DeleteProcess extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!request.getParameter("id").isBlank()) {
            String id = request.getParameter("id");
            Connection c = null;
            try {
                c = DBConnection.getConnection();
                c.createStatement().executeUpdate("DELETE FROM `user` WHERE `id` = '" + id + "'");
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            response.sendRedirect("home.jsp");
        }
    }
}
