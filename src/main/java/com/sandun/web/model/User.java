package com.sandun.web.model;

public class User {


    private String id;
    private String name;
    private String password;

    public User(String id, String name, String password) {
        this.id = id;
        this.password = password;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }
}
