package com.sandun.web.model;

import com.sandun.web.db.DBConnection;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "SignUpProcess", value = "/SignUpProcess")
public class SignUpProcess extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ServletContext context = request.getServletContext();
        if (!request.getParameter("name").isBlank() && !request.getParameter("password").isBlank()) {
            Connection c = null;
            try {
                c = DBConnection.getConnection();
                ResultSet rs = c.createStatement().executeQuery("SELECT * FROM `user` WHERE `name`='" + request.getParameter("name") + "' AND `password`='" + request.getParameter("password") + "' ;");
                if (!rs.next()) {
                    c.createStatement().executeUpdate("INSERT INTO `user` (`name`,`password`) VALUES ('" + request.getParameter("name") + "','" + request.getParameter("password") + "')");
                    response.sendRedirect("index.jsp");
                }else {
                    response.getWriter().write("User Already Exists");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            response.getWriter().write("Please enter fill the text field to create user");
        }
    }
}
