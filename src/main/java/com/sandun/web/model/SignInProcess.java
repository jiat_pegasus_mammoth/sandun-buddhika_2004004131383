package com.sandun.web.model;

import com.sandun.web.db.DBConnection;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet(name = "SignInProcess", value = "/SignInProcess")
public class SignInProcess extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (!request.getParameter("name").isBlank() && !request.getParameter("password").isBlank()) {
            Connection c = null;
            try {
                c = DBConnection.getConnection();
                System.out.println(c);
                System.out.println("Helloasdasdasdas");
                ResultSet rs = c.createStatement().executeQuery("SELECT * FROM `user` WHERE `name`='" + request.getParameter("name") + "' AND `password`='" + request.getParameter("password") + "' ;");
                if (rs.next()) {
                    HttpSession session = request.getSession();
                    User u = new User(rs.getString("id"), rs.getString("name"), rs.getString("password"));
                    session.setAttribute("u", u);
                    response.sendRedirect("home.jsp");
                } else {
                    response.getWriter().write("Incorrect details");
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if(c!=null){
                        c.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } else {
            response.getWriter().write("Please enter fill the text field to Log in");
        }
    }
}
