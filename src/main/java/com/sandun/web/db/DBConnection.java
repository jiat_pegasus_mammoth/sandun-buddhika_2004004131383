package com.sandun.web.db;

import com.sandun.web.util.ApplicationProperties;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBConnection {

    private static Connection connection;

    public static Connection getConnection() throws Exception {
        ApplicationProperties ap = ApplicationProperties.getInstance();
        Class.forName(ap.get("sql.connection.driver"));
        connection = DriverManager.getConnection(ap.get("sql.connection.url"), ap.get("sql.connection.name"), ap.get("sql.connection.password"));
        return connection;
    }

}
